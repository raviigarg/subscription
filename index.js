const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const expressValidator = require('express-validator');
const session = require('express-session');
const config = require('./config/database');
const passport = require('passport');
const flash = require('connect-flash');

mongoose.connect(config.database);
let db = mongoose.connection;

db.once('open', ()=> {
    console.log('Connected to MongoDb');
});

db.on('error', (err) => {
    console.log(err);
});

const app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(`${__dirname}/public`));

app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}));

app.use(require('connect-flash')());
app.use((req, res, next) => {
    res.locals.messages = require('express-messages')(req,res);
    next();
});

app.use(expressValidator( {
    expressValidator: (param, msg, value) => {
        var namespace = param.split('.'),
        root = namespace.shift(),
        formParam = root; 

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }

        return {
            param: formParam,
            msg: msg,
            value: value 
        };
    }
}));

require('./config/passport')(passport);

app.use(passport.initialize());
app.use(passport.session());

app.get('*', (req, res, next) => {
    res.locals.user = req.user || null;
    next();
  });

app.get('/', (req, res) => {
    if(req.user) {
        res.render('index');
    } else {
        res.render('login');
    }
});

app.get('/subscription', (req, res) => {
    res.render('subscription');
});

let users = require('./routes/users');
app.use('/users', users);
let payment = require('./routes/payment');
app.use('/payment', payment);

const port = process.env.PORT || 5000;

app.listen(port,  () => {
    console.log(`Server started on port ${port}`);
});
