const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    customerId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    credits: {
        type: Number,
        required: true
    },
    activeSubscription: {
        type: String
    },
    startDate: {
        type: Date
    },
    endDate: {
        type: Date
    }
});

const User = module.exports = mongoose.model('User', UserSchema);
