const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const config = require('../config/database');
const stripe = require('stripe')('sk_test_7IRRWl1ysI4XDGh3CrLT7qm6');

mongoose.connect(config.database);
let db = mongoose.connection;

router.get('/success', (req ,res) => {
    if(req.user) {
        res.render('success');
    }
    else {
        res.render('error');
    }
});

router.get('/failed', (req ,res) => {
    if(req.user) {
        res.render('failed');
    }
    else {
        res.render('error');
    }
});

let User = require('../models/user');

router.post('/charge-angular', (req, res) => {
    const amount = 900;

    stripe.customers.update(req.user.customerId, {
        source: req.body.stripeToken,
    }, (err, customer) => {
        if(err) {
            res.redirect('/payment/failed');
        }
        stripe.charges.create({
            amount,
            description: 'Web Development course 1',
            currency: 'usd',
            customer: customer.id,
        }, (err, charge) => {
            if(err) {
                res.redirect('/payment/failed');
            } else {
                let newCredits = req.user.credits + 10;
                let newStartDate = new Date();
                let newEndDate = new Date();
                newEndDate.setDate(newEndDate.getDate()+30);
                let newSubscription = " Web Development Angular Course ($9/Month)";
                db.collection('users').updateOne (
                    { _id : req.user._id },
                    { $set : { 
                        credits: newCredits,
                        startDate: newStartDate,
                        endDate: newEndDate,
                        activeSubscription: newSubscription
                    } },
                    function( err, result ) {
                        if ( err ) throw err;
                    }
                );
                res.redirect('/payment/success');
            }
        });
    });
});

router.post('/charge-node', (req, res) => {
    const amount = 1900;

    stripe.customers.update(req.user.customerId, {
        source: req.body.stripeToken,
    }, (err, customer) => {
        if(err) {
            res.redirect('/payment/failed');
        }
        stripe.charges.create({
            amount,
            description: 'Web Development course 2',
            currency: 'usd',
            customer: req.user.customerId
        }, (err, charge) => {
            if(err) {
                res.redirect('/payment/failed');
            } else {
                let newCredits = req.user.credits + 20;
                let newStartDate = new Date();
                let newEndDate = new Date();
                newEndDate.setDate(newEndDate.getDate()+30);
                let newSubscription = " Web Development NodeJs Course ($19/Month)";
                db.collection('users').updateOne (
                    { _id : req.user._id },
                    { $set : { 
                        credits: newCredits,
                        startDate: newStartDate,
                        endDate: newEndDate,
                        activeSubscription: newSubscription
                    } },
                    function( err, result ) {
                        if ( err ) throw err;
                    }
                );
                res.redirect('/payment/success');
            }
        });
    });
});

module.exports = router;