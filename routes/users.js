const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const stripe = require('stripe')('sk_test_7IRRWl1ysI4XDGh3CrLT7qm6');

let User = require('../models/user');

router.get('/register', (req, res) => {
    if(req.user) {
        res.render('error');
    } else {
        res.render('register');
    }
});

router.post('/register', (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const passwordc = req.body.passwordc;

    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('passwordc', 'Password do not match').equals(req.body.password);

    let errors = req.validationErrors();

    if(errors) {
        res.render('register', {
            errors: errors
        });
    } else {
        let newUser = new User({
            name: name,
            email: email,
            password: password,
            credits: 0
        });
        stripe.customers.create({
            email: email,
          }, (err, customer) => {
              if(err) {
                  console.log(err);
              } else {
                newUser.customerId = customer.id,
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if(err) {
                            console.log(err);
                        }
                        newUser.password = hash;
                        newUser.save((err) => {
                            if(err) {
                                console.log(err);
                                return;
                            } else {
                                res.redirect('/users/login');
                            }
                        });
                    });
                }); 
              }
          });
    }
});

router.get('/login', (req,res) => {
    if(req.user) {
        res.render('error');
    } else {
        res.render('login');
    }
    
});

router.get('/profile', (req, res) => {
    if(req.user) {
        res.render('profile');
    } else {
        res.render('error'); 
    }
});

router.get('/subscription', (req, res) => {
    if(req.user) {
        res.render('subscription');
    } else {
        res.render('error'); 
    }
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
      successRedirect:'/',
      failureRedirect:'/users/login',
      failureFlash: true
    })(req, res, next);
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/users/login');
});
module.exports = router;